# ci-docker-images

```
beagle-devscripts-<os>-<arch>:latest
```

Base install: `apindex build-essential debhelper-compat devscripts`

# debian-10

```
image: robertcnelson/beagle-devscripts-debian-10-armhf:latest
```

# debian-11

```
image: robertcnelson/beagle-devscripts-debian-11-armhf:latest
image: robertcnelson/beagle-devscripts-debian-11-arm64:latest
```

# debian-12

```
image: robertcnelson/beagle-devscripts-debian-12-armhf:latest
image: robertcnelson/beagle-devscripts-debian-12-arm64:latest
```

# debian-sid

```
image: robertcnelson/beagle-devscripts-debian-sid-riscv64:latest
```

# ubuntu-24.04

```
image: robertcnelson/beagle-devscripts-ubuntu-24.04-riscv64:latest
```

# Special, debian-12 + tools to build kernel:

```
image: robertcnelson/beagle-devscripts-kernel-debian-12-amd64:latest
image: robertcnelson/beagle-devscripts-kernel-debian-12-arm64:latest
```

# Special, debian-13 + tools to build kernel:

```
image: robertcnelson/beagle-devscripts-kernel-debian-13-amd64:latest
image: robertcnelson/beagle-devscripts-kernel-debian-13-arm64:latest
image: robertcnelson/beagle-devscripts-kernel-debian-13-armhf:latest
image: robertcnelson/beagle-devscripts-kernel-debian-13-riscv64:latest
```
