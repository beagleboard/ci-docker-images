ARG FRM='debian:trixie-slim'
ARG TAG='latest'

FROM ${FRM}
ARG FRM
ARG TAG

RUN mkdir -p /etc/apt/apt.conf.d/ \
	&& echo "Acquire::http::Proxy \"http://192.168.1.10:3142\";" > /etc/apt/apt.conf.d/00aptproxy

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
			--no-install-recommends \
		bash	\
		bc	\
		bindgen	\
		bison	\
		build-essential	\
		ccache	\
		cpio	\
		debhelper	\
		debhelper-compat	\
		device-tree-compiler	\
		devscripts	\
		fakeroot	\
		flex	\
		gcc-aarch64-linux-gnu	\
		gcc-arm-linux-gnueabihf	\
		gcc-riscv64-linux-gnu	\
		git	\
		kmod	\
		libc6-dev	\
		libc6-dev-arm64-cross	\
		libc6-dev-armhf-cross	\
		libc6-dev-riscv64-cross	\
		libelf-dev	\
		libmpc-dev	\
		libncurses-dev	\
		libssl-dev	\
		lsb-release	\
		lz4	\
		lzma	\
		lzop	\
		man-db	\
		openssl	\
		pkg-config	\
		python3-dev	\
		python3-pip	\
		rsync	\
		rustc	\
		rustfmt	\
		tree	\
		u-boot-tools	\
		unzip	\
		wget	\
		zstd	\
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& git config --global user.name "GitLab CI" \
	&& git config --global user.email "beagle@beagleboard.org" \
	&& apt-get clean -y \
	; rm -rf \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

RUN rm /etc/apt/apt.conf.d/00aptproxy

RUN echo "deb [trusted=yes] http://repos.rcn-ee.net/debian-amd64/ trixie main" > /etc/apt/sources.list.d/beagle.list \
	&& echo "deb-src [trusted=yes] http://repos.rcn-ee.net/debian-amd64/ trixie main" >> /etc/apt/sources.list.d/beagle.list \
	&& apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
			--no-install-recommends \
		bbb.io-keyring	\
	&& cp -v /etc/bbb.io/templates/apt/sbuild.list /etc/apt/sources.list.d/beagle.list

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
			--no-install-recommends \
		apindex	\
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y \
	&& DEBIAN_FRONTEND=noninteractive apt-get autoremove -y \
	&& apt-get clean -y \
	; rm -rf \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

RUN wget --directory-prefix=/etc/ssl/certs/ http://192.168.1.98/mirror/internal/git.gfnd.rcn-ee.org.2024.crt \
	&& mv -v /etc/ssl/certs/git.gfnd.rcn-ee.org.2024.crt /etc/ssl/certs/git.gfnd.rcn-ee.org.crt \
	&& chmod 644 /etc/ssl/certs/git.gfnd.rcn-ee.org.crt \
	&& c_rehash /etc/ssl/certs

RUN wget --directory-prefix=/etc/ssl/certs/ http://192.168.1.98/mirror/internal/gitlab.gfnd.rcn-ee.org.crt \
	&& chmod 644 /etc/ssl/certs/gitlab.gfnd.rcn-ee.org.crt \
	&& c_rehash /etc/ssl/certs

