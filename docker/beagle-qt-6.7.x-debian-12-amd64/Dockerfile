ARG FRM='debian:bookworm-slim'
ARG TAG='latest'

FROM ${FRM}
ARG FRM
ARG TAG

RUN mkdir -p /etc/apt/apt.conf.d/ \
	&& echo "Acquire::http::Proxy \"http://192.168.1.10:3142\";" > /etc/apt/apt.conf.d/00aptproxy

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
			--no-install-recommends \
		build-essential	\
		cmake	\
		debhelper	\
		devscripts	\
		git	\
		libarchive-dev	\
		libcurl4-gnutls-dev	\
		libdbus-1-dev	\
		libegl-dev	\
		libfontconfig-dev	\
		libfreetype-dev	\
		libfuse-dev	\
		libglib2.0-dev	\
		libgnutls28-dev	\
		libwayland-cursor0	\
		libwayland-egl1	\
		libxcb-cursor0	\
		libxcb-icccm4	\
		libxcb-keysyms1	\
		libxcb-randr0	\
		libxcb-shape0	\
		libxkbcommon-dev	\
		libxkbcommon-x11-0	\
		locales	\
		locales-all	\
		mesa-common-dev	\
		patchelf	\
		python3-pip	\
		python3-venv	\
		tree	\
		wget	\
	&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
	&& git config --global user.name "GitLab CI" \
	&& git config --global user.email "beagle@beagleboard.org" \
	&& apt-get clean -y \
	; rm -rf \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN pip install aqtinstall --break-system-packages

#https://ddalcino.github.io/aqt-list-server/

ARG QT_HOST=linux
ARG QT_TARGET=desktop
ARG QT=6.7.2
ARG QT_ARCH=linux_gcc_64

RUN aqt install-qt --outputdir /opt/qt ${QT_HOST} ${QT_TARGET} ${QT} ${QT_ARCH}
RUN aqt install-tool --outputdir /opt/qt ${QT_HOST} ${QT_TARGET} tools_cmake
RUN aqt install-tool --outputdir /opt/qt ${QT_HOST} ${QT_TARGET} tools_qtcreator

ENV PATH /opt/qt/${QT}/gcc_64/bin:$PATH
ENV QT_PLUGIN_PATH /opt/qt/${QT}/gcc_64/plugins/
ENV QML_IMPORT_PATH /opt/qt/${QT}/gcc_64/qml/
ENV QML2_IMPORT_PATH /opt/qt/${QT}/gcc_64/qml/
