# Build

```
docker build --no-cache -t robertcnelson/beagle-devscripts-kernel-debian-12-arm64 .
docker push robertcnelson/beagle-devscripts-kernel-debian-12-arm64:latest
```
