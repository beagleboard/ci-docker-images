# Build

```
docker build --no-cache -t robertcnelson/beagle-devscripts-ubuntu-24.04-riscv64 .
docker push robertcnelson/beagle-devscripts-ubuntu-24.04-riscv64:latest
```
