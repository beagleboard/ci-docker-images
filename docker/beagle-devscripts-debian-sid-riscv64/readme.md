# Build

```
docker build --no-cache -t robertcnelson/beagle-devscripts-debian-sid-riscv64 .
docker push robertcnelson/beagle-devscripts-debian-sid-riscv64:latest
```
