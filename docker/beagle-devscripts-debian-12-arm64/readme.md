# Build

```
docker build --no-cache -t robertcnelson/beagle-devscripts-debian-12-arm64 .
docker push robertcnelson/beagle-devscripts-debian-12-arm64:latest
```
