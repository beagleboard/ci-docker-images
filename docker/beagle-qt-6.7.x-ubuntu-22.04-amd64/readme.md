# Build

```
docker build --no-cache -t robertcnelson/beagle-qt-6.7.x-ubuntu-22.04-amd64 .
docker push robertcnelson/beagle-qt-6.7.x-ubuntu-22.04-amd64:latest
```
