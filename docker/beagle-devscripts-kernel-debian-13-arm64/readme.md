# Build

```
docker build --no-cache -t robertcnelson/beagle-devscripts-kernel-debian-13-arm64 .
docker push robertcnelson/beagle-devscripts-kernel-debian-13-arm64:latest
```
