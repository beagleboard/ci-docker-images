# Build

```
docker build --no-cache -t robertcnelson/nvidia-cuda-12.5.0-devel-ubuntu20.04-anaconda .
docker push robertcnelson/nvidia-cuda-12.5.0-devel-ubuntu20.04-anaconda:latest
```
